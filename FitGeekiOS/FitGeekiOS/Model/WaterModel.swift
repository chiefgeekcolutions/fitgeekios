//
//  WaterModel.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 10/29/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import Foundation

struct Water: Decodable {
    var wtr_key: String
    var wtr_usr_key: String
    var wtr_start_datetime: Date?
    var wtr_end_datetime: Date?
    var wtr_gallon_total: Int?
    var wtr_add_date: String?
    var wtr_add_user: String?
    var wtr_change_date: String?
    var wtr_change_user: String?
    var wtr_delete_flag: Int?
    
    init(key: String?, usr_key: String){
        let oDF = DateFormatter()
        if key!.isEmpty {
            // Create New Ped Object
            self.wtr_key = UUID().uuidString
            self.wtr_usr_key = usr_key
            self.wtr_add_date = oDF.string(from: Date.init())
            self.wtr_add_user = "iOS App"
        } else {
            // TODO: Call server to get Wtr by key then set object
            self.wtr_usr_key = usr_key
            self.wtr_key = ""
        }
    }
}
