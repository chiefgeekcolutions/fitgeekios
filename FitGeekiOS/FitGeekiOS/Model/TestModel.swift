//
//  TestModel.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 10/29/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import Foundation

struct TestModel: Decodable {
    var value_1: String
    var value_2: String
}
