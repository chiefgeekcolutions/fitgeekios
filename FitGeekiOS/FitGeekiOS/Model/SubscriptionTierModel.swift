//
//  SubscriptionTierModel.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 10/29/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import Foundation

struct SubscriptionTier: Decodable {
    var sbt_key: String
    var sbt_code: String
    var sbt_add_date: String?
    var sbt_add_user: String?
    var sbt_change_date: String?
    var sbt_change_user: String?
    var sbt_delete_flag: Int?
    
    init(key: String?, code: String){
        let oDF = DateFormatter()
        if key!.isEmpty {
            // Create New Ped Object
            self.sbt_key = UUID().uuidString
            self.sbt_code = code
            self.sbt_add_date = oDF.string(from: Date.init())
            self.sbt_add_user = "iOS App"
        } else {
            // TODO: Call server to get Wtr by key then set object
            self.sbt_code = code
            self.sbt_key = ""
        }
    }
}
