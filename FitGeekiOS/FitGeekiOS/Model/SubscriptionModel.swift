//
//  SubscriptionModel.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 10/29/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import Foundation
struct Subscription: Decodable {
    var sub_key: String
    var sub_usr_key: String
    var sub_name: String?
    var sub_sbt_key: String?
    var sub_price: Double?
    var sub_add_date: String?
    var sub_add_user: String?
    var sub_change_date: String?
    var sub_change_user: String?
    var sub_delete_flag: Int?
    
    init(key: String?, usr_key: String){
        let oDF = DateFormatter()
        if key!.isEmpty {
            // Create New Ped Object
            self.sub_key = UUID().uuidString
            self.sub_usr_key = usr_key
            self.sub_add_date = oDF.string(from: Date.init())
            self.sub_add_user = "iOS App"
        } else {
            // TODO: Call server to get Wtr by key then set object
            self.sub_usr_key = usr_key
            self.sub_key = ""
        }
    }
}
