//
//  Pedometer.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 10/29/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import Foundation

struct Pedometer: Decodable {
    var ped_key: String
    var ped_usr_key: String
    var ped_start_datetime: Date?
    var ped_end_datetime: Date?
    var ped_steps_total: Int?
    var ped_steps_per_hour: Int?
    var ped_add_date: String?
    var ped_add_user: String?
    var ped_change_date: String?
    var ped_change_user: String?
    var ped_delete_flag: Int?
    
    init(ped_key: String?, ped_usr_key: String) {
        let oDF = DateFormatter()
        // TODO: Get User if ped key exists/create new if empty
        if ped_key!.isEmpty {
            // Create New Ped Object
            self.ped_key = UUID().uuidString
            self.ped_usr_key = ped_usr_key
            self.ped_add_date = oDF.string(from: Date.init())
            self.ped_add_user = "iOS App"
        } else {
            // TODO: Call server to get Ped by key then set object
            self.ped_usr_key = ped_usr_key
            self.ped_key = ""
        }
    }
}
