//
//  Subscription_X_User.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 10/29/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import Foundation

struct Subscription_X_User: Decodable {
    var sxu_key: String
    var sxu_usr_key: String
    var sxu_sub_key: String?
    var sxu_first_start_date: String?
    var sxu_start_date: Date?
    var sxu_expire_date: Date?
    var sxu_add_date: String?
    var sxu_add_user: String?
    var sxu_change_date: String?
    var sxu_change_user: String?
    var sxu_delete_flag: Int?
    
    init(key: String?, user_key: String){
        let oDF = DateFormatter()
        if key!.isEmpty {
            // Create New Ped Object
            self.sxu_key = UUID().uuidString
            self.sxu_usr_key = user_key
            self.sxu_add_date = oDF.string(from: Date.init())
            self.sxu_add_user = "iOS App"
        } else {
            // TODO: Call server to get Wtr by key then set object
            self.sxu_usr_key = user_key
            self.sxu_key = ""
        }
    }
}
