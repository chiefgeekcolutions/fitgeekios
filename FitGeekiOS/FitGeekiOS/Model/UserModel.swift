//
//  User.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 10/28/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import Foundation

struct User: Codable {
    var usr_key: String
    var usr_first_name: String?  = ""
    var usr_last_name: String?  = ""
    var usr_email_address: String?  = ""
    var usr_username: String?  = ""
    var usr_bio: String?  = ""
    var usr_password: String?  = ""
    var usr_add_date: String?  = ""
    var usr_add_user: String?  = ""
    var usr_change_date: String?  = ""
    var usr_change_user: String? = ""
    var usr_delete_flag: Int? = 0
    
    init(key: String?) {
        let oDF = DateFormatter()
        
        if key!.isEmpty {
            self.usr_key = UUID().uuidString
            self.usr_add_user = "iOS App"
            self.usr_add_date = oDF.string(from: Date.init())
        } else {
            // TODO: Get User form service
            self.usr_key = key!
        }
        
    }
    
    enum CodingKeys: String, CodingKey {
        case usr_key
        case usr_first_name
        case usr_last_name
        case usr_email_address
        case usr_username
        case usr_bio
        case usr_password
        case usr_add_date
        case usr_add_user
        case usr_change_date
        case usr_change_user
        case usr_delete_flag
    }
    
    func toJSON() -> Any? {
        /*let props: Dictionary<String, Any> = ["usr_key": usr_key,
                     "usr_first_name": usr_first_name!,
                     "usr_last_name": usr_last_name!,
                     "usr_email_address": usr_email_address!,
                     "usr_username": usr_username!,
                     "usr_bio": usr_bio!,
                     "usr_add_date": usr_add_date!,
                     "usr_add_user": usr_add_user!,
                     "usr_change_date": usr_change_date!,
                     "usr_change_user": usr_change_user!,
                     "usr_delete_flag": usr_delete_flag!
            ] //as [String : Any]*/
        do {
            let rawJson = try JSONEncoder().encode(self)//encode(self)
            let jsonObject = try JSONSerialization.jsonObject(with: rawJson, options: [])
            //let jsonData = //try JSONSerialization.data(withJSONObject: rawJson,
                                                      //options: .prettyPrinted)
            return jsonObject//String(data: jsonData, encoding: String.Encoding.utf8)
        } catch let error {
            print("error converting to json: \(error)")
            return nil
        }
    }
    
}
