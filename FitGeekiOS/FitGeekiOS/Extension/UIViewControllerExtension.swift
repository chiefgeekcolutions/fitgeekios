//
//  UIViewControllerExtension.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 12/14/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import Foundation
import UIKit

// Put this piece of code anywhere you like
extension UIViewController {
  func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func fixKeyboardIssues() {
        UIApplication.shared.keyWindow?.backgroundColor = UIColor.white
    }
    
}
