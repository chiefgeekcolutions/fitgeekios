//
//  UITextFieldExtension.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 12/13/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    func loadDropdownData(data: [String]) {
        self.inputView = DropDownPickerView(pickerData: data, dropdownField: self)
    }
}
