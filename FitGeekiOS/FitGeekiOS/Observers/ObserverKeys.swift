//
//  ObserverKeys.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 1/16/18.
//  Copyright © 2018 Jeremy Evans. All rights reserved.
//
//  Contant Observer Keys to use with Notifcation and Observers
//

import Foundation
import UIKit

// Constant Unique portion of Keys
let uniqueId = "com.fitgeekapp."

// Keys
let signUpToAppKey = uniqueId + "SignUp"
let loginToAppKey = uniqueId + "login"
