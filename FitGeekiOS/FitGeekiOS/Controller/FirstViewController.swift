//
//  FirstViewController.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 10/29/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    
    // Keys
    let loginName = Notification.Name(rawValue: loginToAppKey)
    let signupName = Notification.Name(rawValue: signUpToAppKey)
    
    // Utils
    let oImgUtil = ImageUtils()
    
    // View
   let LogoImageView: AspectFitImageView = {
        let ImageView = AspectFitImageView(image: #imageLiteral(resourceName: "OrangeHead"))
    // this enables autolayout for our imageview
        ImageView.translatesAutoresizingMaskIntoConstraints = false
        return ImageView
    }()
    
    let LogoTextImage: AspectFitImageView = {
        let ImageView = AspectFitImageView(image: #imageLiteral(resourceName: "textLogoWhite"))
        // this enables autolayout for our imageview
        ImageView.translatesAutoresizingMaskIntoConstraints = false
        return ImageView
    }()
    
    let signUpButton: OrangeButton = {
        let ButtonSize = CGRect(x: 0, y: 0, width: 250, height: 45)
        let button = OrangeButton(frame: ButtonSize, title: "Signup")
        
        button.addTarget(self, action: #selector(signUpButtonAction), for: .touchUpInside)
        // this enables autolayout for our imageview
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let loginButton: DefaultButton = {
        let ButtonSize = CGRect(x: 0, y: 0, width: 250, height: 45)
        let button = DefaultButton(frame: ButtonSize, title: "Login")
        button.addTarget(self, action: #selector(loginButtonAction), for: .touchUpInside)
        // this enables autolayout for our imageview
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    // Views End

    override func viewDidLoad() {
        super.viewDidLoad()
        // Here do everything needed to do in the viewDidLoad
        
        // BG Image
        let bgImage = oImgUtil.imageWithImage(image: UIImage(named: "First View Background.png")!, scaledToSize: CGSize(width: self.view!.bounds.width, height: self.view!.bounds.height))
        
        // Set Background
        self.view.backgroundColor = UIColor(patternImage: bgImage)
        
        // Add all controls to subview
        self.view.addSubview(LogoImageView)
        self.view.addSubview(LogoTextImage)
        self.view.addSubview(signUpButton)
        self.view.addSubview(loginButton)
        
        // SetUp Layout
        setUpLayout()
        
        // After UI is Setup
        setObservers()
    }
    
    // Observers
    func setObservers() {
        
        // Login Observer
        NotificationCenter.default.addObserver(self, selector: #selector(LoginMethods.loginToFitGeek(notification:)), name: loginName, object: nil)
        
        // Signup Observer
        NotificationCenter.default.addObserver(self, selector: #selector(signupToFitGeek(notification:)), name: signupName, object: nil)
    }
    
    // Selector methods
    @objc func signupToFitGeek(notification: NSNotification) {
        var oErr = ErrorClass()
        let rawUser = notification.object as! User
        
        // Send to API
        oErr = ApiUtils.SignUpUser(newUser: rawUser)
        
        // Login User
        if(oErr.err_message == "")
        {
            NotificationCenter.default.post(name: loginName, object: nil) // set to newUser when this s atted
        }
    }
    
    @objc public static func loginToFitGeek(notification: NSNotification) {
        // Do Login Here
        print("made it to login")
    }
    // Selector Methods End
    
    deinit {
        // Deinitialize all observers
        NotificationCenter.default.removeObserver(self)
    }
    
    // Button Actions
    @objc func signUpButtonAction(sender: UIButton!) {
        let signupVC = SignupViewController()
        /// Work On transitions after functionality is done
        self.present(signupVC, animated: false) {
            // Do Nothing
        }
    }
    
    @objc func loginButtonAction(sender: UIButton!) {
        print("Login tapped")
        let loginVC = LoginViewController()
        
        /// Work On transitions after functionality is done
        self.present(loginVC, animated: false) {
            // Do Nothing
        }
    }
    
    // Observer Methods

    
    // Layout Start
    private func setUpLayout() {
        // Auto Layout
        // Logo Image View
        //LogoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        LogoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -10).isActive = true
        LogoImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 110).isActive = true
        LogoImageView.widthAnchor.constraint(equalToConstant: 96).isActive = true
        LogoImageView.heightAnchor.constraint(equalToConstant: 115).isActive = true
        
        // FitgeekText
        LogoTextImage.topAnchor.constraint(equalTo: LogoImageView.bottomAnchor).isActive = true
        LogoTextImage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        LogoTextImage.widthAnchor.constraint(equalToConstant: 214).isActive = true
        LogoTextImage.heightAnchor.constraint(equalToConstant: 66).isActive = true
        
        // Signup Buttom
        signUpButton.topAnchor.constraint(equalTo: LogoTextImage.bottomAnchor, constant: 89).isActive = true
        signUpButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        signUpButton.widthAnchor.constraint(equalToConstant: 250).isActive = true
        signUpButton.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        // Login Button
        loginButton.topAnchor.constraint(equalTo: signUpButton.bottomAnchor, constant: 25).isActive = true
        loginButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loginButton.widthAnchor.constraint(equalToConstant: 250).isActive = true
        loginButton.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
    }
    // Layout End
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    // Helper Functions End
}
