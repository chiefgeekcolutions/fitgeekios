//
//  ViewController.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 10/28/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Get Set FitGeekApiInfo
        var FitGeekInfo: Dictionary<String, Any>?
        // Test Get PList Info
        if let path = Bundle.main.path(forResource: "FitGeekApi", ofType: "plist") {
            //print(path)
            FitGeekInfo = NSDictionary(contentsOfFile: path) as? Dictionary
        }
        
        //Implementing URLSession
        if let ApiInfo = FitGeekInfo {
        let urlString = ApiInfo["apiurl"] as! String + "Pedometer"
            print(urlString)
            
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            
            guard let data = data else { return }
            
            //Implement JSON decoding and parsing
            do {
                //Decode retrived data with JSONDecoder and assing type of Article object
                let testJson = try JSONDecoder().decode(TestModel.self, from: data)//try JSONDecoder().decode.(String, from: data)
                print(testJson.value_1)
                print("\n")
                print(testJson.value_2)
                
            } catch let jsonError {
                print(jsonError)
            }
            
            }.resume()
        }
        //End implementing URLSession
        
    }
}

