//
//  SignupViewController.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 11/4/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController {//: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    private let oImgUtil = ImageUtils()
    private let pages = 3
    private var collectionViewSizeChanged: Bool = false
    //internal var collectionView: UICollectionView!
    private var flowLayout: UICollectionViewFlowLayout!
    private let margin: CGFloat = 20.0
    
    // Controls
    let logoImageView: AspectFitImageView = {
        let imageView = AspectFitImageView(image: #imageLiteral(resourceName: "DarkHeadLogo"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let NameLabel: OrangeLabel = {
        let label = OrangeLabel(frame: CGRect(x: 0, y: 0, width: 35, height: 16))
        label.text = "Full Name:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let UserNameLabel: OrangeLabel = {
        let label = OrangeLabel(frame: CGRect(x: 0, y: 0, width: 35, height: 16))
        label.text = "Username:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let EmailLabel: OrangeLabel = {
        let label = OrangeLabel(frame: CGRect(x: 0, y: 0, width: 35, height: 16))
        label.text = "Email:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let PasswordLabel: OrangeLabel = {
        let label = OrangeLabel(frame: CGRect(x: 0, y: 0, width: 100, height: 16))
        label.text = "Password:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let PasswordConfirmLabel: OrangeLabel = {
        let label = OrangeLabel(frame: CGRect(x: 0, y: 0, width: 100, height: 16))
        label.text = "Confirm Password:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let NameTxtField: BottomBorderTextField = {
        let rect = CGRect(x: 0, y: 0, width: 300, height: 45)
        let txtField = BottomBorderTextField(frame: rect, PHolderTxt: "John Doe")
        txtField.translatesAutoresizingMaskIntoConstraints = false
        return txtField
    }()
    
    let UsernameTxtField: BottomBorderTextField = {
        let rect = CGRect(x: 0, y: 0, width: 300, height: 45)
        let txtField = BottomBorderTextField(frame: rect, PHolderTxt: "uknwitzremy")
        txtField.translatesAutoresizingMaskIntoConstraints = false
        return txtField
    }()
    
    let EmailTxtField: BottomBorderTextField = {
        let rect = CGRect(x: 0, y: 0, width: 300, height: 45)
        let txtField = BottomBorderTextField(frame: rect, PHolderTxt: "e.g. jdoe@gmail.com")
        txtField.translatesAutoresizingMaskIntoConstraints = false
        return txtField
    }()
    
    let PwdTxtField: BottomBorderTextField = {
        let rect = CGRect(x: 0, y: 0, width: 300, height: 45)
        let txtField = BottomBorderTextField(frame: rect, PHolderTxt: "**************")
        txtField.isSecureTextEntry = true
        txtField.translatesAutoresizingMaskIntoConstraints = false
        return txtField
    }()
    
    let PwdConfirmTxtField: BottomBorderTextField = {
        let rect = CGRect(x: 0, y: 0, width: 300, height: 45)
        let txtField = BottomBorderTextField(frame: rect, PHolderTxt: "**************")
        txtField.isSecureTextEntry = true
        txtField.translatesAutoresizingMaskIntoConstraints = false
        
        return txtField
    }()
    
    // Submit Button
    let SubmitButton: OrangeButton = {
        let ButtonSize = CGRect(x: 0, y: 0, width: 250, height: 45)
        let button = OrangeButton(frame: ButtonSize, title: "Submit")
        // this enables autolayout for our imageview
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(submitTapped), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set background
        let bgImage = oImgUtil.imageWithImage(image: UIImage(named: "LoginBG.png")!, scaledToSize: CGSize(width: self.view.bounds.width, height: self.view.bounds.height))
        
        // Set Background
        self.view.backgroundColor = UIColor(patternImage: bgImage)
        // layout
        setupLayout()
        //Looks for single or multiple taps.
        self.hideKeyboardWhenTappedAround()
    }
    
    // Button Actions
    @objc func submitTapped(sender: UIButton) {
        
        let nameArray = NameTxtField.text?.trimmingCharacters(in: .whitespacesAndNewlines).components(separatedBy: " ")
        
        // Create Object from what is set
        var oUser = User(key: "")
        
        guard let array = nameArray else {
            return
        }
        
        oUser.usr_first_name = array[0]
        oUser.usr_last_name = array[1]
        oUser.usr_email_address = EmailTxtField.text
        oUser.usr_username = UsernameTxtField.text
        if(PwdTxtField.text == PwdConfirmTxtField.text)
        {
            oUser.usr_password = PwdTxtField.text
        }
        //print(oUser)
        
        // Post Notification
        let nName = Notification.Name(rawValue: signUpToAppKey)
        NotificationCenter.default.post(name: nName, object: oUser)
        
        // Finally: Dismiss VC
        dismiss(animated: false, completion: nil)
    }
    
    func setupLayout(){
        // Add Views to Subview
        self.view.addSubview(logoImageView)
        self.view.addSubview(NameLabel)
        self.view.addSubview(NameTxtField)
        self.view.addSubview(UserNameLabel)
        self.view.addSubview(UsernameTxtField)
        self.view.addSubview(EmailLabel)
        self.view.addSubview(EmailTxtField)
        self.view.addSubview(PasswordLabel)
        self.view.addSubview(PwdTxtField)
        self.view.addSubview(PasswordConfirmLabel)
        self.view.addSubview(PwdConfirmTxtField)
        self.view.addSubview(SubmitButton)
        
        // Logo
        logoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -10).isActive = true
        logoImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 30).isActive = true
        logoImageView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        logoImageView.heightAnchor.constraint(equalToConstant: 96).isActive = true
        
        // First Name label
        NameLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 40).isActive = true
        NameLabel.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 25).isActive = true
        NameLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        NameLabel.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        // First Name Textbox
        NameTxtField.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        NameTxtField.topAnchor.constraint(equalTo: NameLabel.bottomAnchor, constant: 0).isActive = true
        NameTxtField.widthAnchor.constraint(equalToConstant: 300).isActive = true
        NameTxtField.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        // Last Name label
        UserNameLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 40).isActive = true
        UserNameLabel.topAnchor.constraint(equalTo: NameTxtField.bottomAnchor, constant: 35).isActive = true
        UserNameLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        UserNameLabel.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        // Last Name Textbox
        UsernameTxtField.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        UsernameTxtField.topAnchor.constraint(equalTo: UserNameLabel.bottomAnchor, constant: 0).isActive = true
        UsernameTxtField.widthAnchor.constraint(equalToConstant: 300).isActive = true
        UsernameTxtField.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        // Email label
        EmailLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 40).isActive = true
        EmailLabel.topAnchor.constraint(equalTo: UsernameTxtField.bottomAnchor, constant: 35).isActive = true
        EmailLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        EmailLabel.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        // Username Textbox
        EmailTxtField.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        EmailTxtField.topAnchor.constraint(equalTo: EmailLabel.bottomAnchor, constant: 0).isActive = true
        EmailTxtField.widthAnchor.constraint(equalToConstant: 300).isActive = true
        EmailTxtField.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        // Password Lable
        PasswordLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 40).isActive = true
        PasswordLabel.topAnchor.constraint(equalTo: EmailTxtField.bottomAnchor, constant: 35).isActive = true
        PasswordLabel.widthAnchor.constraint(equalToConstant: 300).isActive = true
        PasswordLabel.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        // Password TxtField
        PwdTxtField.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        PwdTxtField.topAnchor.constraint(equalTo: PasswordLabel.bottomAnchor, constant: 0).isActive = true
        PwdTxtField.widthAnchor.constraint(equalToConstant: 300).isActive = true
        PwdTxtField.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        // Confirm Password Lable
        PasswordConfirmLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 40).isActive = true
        PasswordConfirmLabel.topAnchor.constraint(equalTo: PwdTxtField.bottomAnchor, constant: 35).isActive = true
        PasswordConfirmLabel.widthAnchor.constraint(equalToConstant: 300).isActive = true
        PasswordConfirmLabel.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        // Confirm Password TxtField
        PwdConfirmTxtField.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        PwdConfirmTxtField.topAnchor.constraint(equalTo: PasswordConfirmLabel.bottomAnchor, constant: 0).isActive = true
        PwdConfirmTxtField.widthAnchor.constraint(equalToConstant: 300).isActive = true
        PwdConfirmTxtField.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        // Submit View
        SubmitButton.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        SubmitButton.topAnchor.constraint(equalTo: PwdConfirmTxtField.bottomAnchor, constant: 35).isActive = true
        SubmitButton.widthAnchor.constraint(equalToConstant: 300).isActive = true
        SubmitButton.heightAnchor.constraint(equalToConstant: 45).isActive = true
    }
}
