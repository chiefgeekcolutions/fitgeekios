//
//  LoginViewController.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 11/1/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    // Controls
    let logoImageView: AspectFitImageView = {
       let imageView = AspectFitImageView(image: #imageLiteral(resourceName: "DarkHeadLogo"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let UserLabel: OrangeLabel = {
       let label = OrangeLabel(frame: CGRect(x: 0, y: 0, width: 35, height: 16))
        label.text = "Email:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let PasswordLabel: OrangeLabel = {
        let label = OrangeLabel(frame: CGRect(x: 0, y: 0, width: 100, height: 16))
        label.text = "Password:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let UserTxtField: BottomBorderTextField = {
        let rect = CGRect(x: 0, y: 0, width: 300, height: 45)
        let txtField = BottomBorderTextField(frame: rect, PHolderTxt: "e.g. jdoe@gmail.com")
        return txtField
    }()
    
    let PwdTxtField: BottomBorderTextField = {
        let rect = CGRect(x: 0, y: 0, width: 300, height: 45)
        let txtField = BottomBorderTextField(frame: rect, PHolderTxt: "**************")
        return txtField
    }()
    
    let forgotPassword: LinkButton = {
        let rect = CGRect(x: 0, y: 0, width: 130, height: 16)
        let btn = LinkButton(frame: rect, text: "Forgot Password?")
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let loginBtn: DefaultButton = {
        let rect = CGRect(x: 0, y: 0, width: 250, height: 45)
        let btn = DefaultButton(frame: rect, title: "Login")
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(loginToApp), for: .touchUpInside)
        return btn
    }()
    // Controls End

    let oImgUtil = ImageUtils()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Key Board Function
        
        // Set background
        let bgImage = oImgUtil.imageWithImage(image: UIImage(named: "LoginBG.png")!, scaledToSize: CGSize(width: self.view!.bounds.width, height: self.view!.bounds.height))
        
        // Set Background
        self.view.backgroundColor = UIColor(patternImage: bgImage)
        
        // Add to subview
        self.view.addSubview(logoImageView)
        self.view.addSubview(UserLabel)
        self.view.addSubview(UserTxtField)
        self.view.addSubview(PasswordLabel)
        self.view.addSubview(PwdTxtField)
        self.view.addSubview(forgotPassword)
        self.view.addSubview(loginBtn)
        
        // Set up layout
        setupLayout()
    }

    // Button Action
    @objc func loginToApp(sender: UIButton!){
        // TODO: Login the user.
        print("Login Tapped")
    }
    
    // layout
    func setupLayout() {
        
        // Logo
        logoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -10).isActive = true
        logoImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 103).isActive = true
        logoImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        logoImageView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        
        // Username label
        UserLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 60).isActive = true
        UserLabel.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 81).isActive = true
        UserLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        UserLabel.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        // Username Textbox
        UserTxtField.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        UserTxtField.topAnchor.constraint(equalTo: UserLabel.bottomAnchor, constant: 5).isActive = true
        UserTxtField.widthAnchor.constraint(equalToConstant: 300).isActive = true
        UserTxtField.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        // Password Label
        PasswordLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 60).isActive = true
        PasswordLabel.topAnchor.constraint(equalTo: UserTxtField.bottomAnchor, constant: 20).isActive = true
        PasswordLabel.widthAnchor.constraint(equalToConstant: 300).isActive = true
        PasswordLabel.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        // Password TxtField
        PwdTxtField.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        PwdTxtField.topAnchor.constraint(equalTo: PasswordLabel.bottomAnchor, constant: 10).isActive = true
        PwdTxtField.widthAnchor.constraint(equalToConstant: 300).isActive = true
        PwdTxtField.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        // Forgot Password btn
        forgotPassword.rightAnchor.constraint(equalTo: PwdTxtField.rightAnchor, constant: 0).isActive = true
        forgotPassword.topAnchor.constraint(equalTo: PwdTxtField.bottomAnchor, constant: 20).isActive = true
        forgotPassword.widthAnchor.constraint(equalToConstant: 130).isActive = true
        forgotPassword.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        // Login Btn
        loginBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        loginBtn.topAnchor.constraint(equalTo: forgotPassword.bottomAnchor, constant: 30).isActive = true
        loginBtn.widthAnchor.constraint(equalToConstant: 250).isActive = true
        loginBtn.heightAnchor.constraint(equalToConstant: 45).isActive = true
    }
}
