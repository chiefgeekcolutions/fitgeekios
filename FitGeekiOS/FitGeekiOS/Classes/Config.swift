//
//  Config.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 1/18/18.
//  Copyright © 2018 Jeremy Evans. All rights reserved.
//

import Foundation
import UIKit


struct Config {
    public static var LastErr: ErrorClass {
        set {
            self.LastErr = newValue //Error
        }
        get {
            return self.LastErr //Error
        }
    }
}
