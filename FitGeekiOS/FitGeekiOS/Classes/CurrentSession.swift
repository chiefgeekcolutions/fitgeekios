//
//  CurrentSession.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 1/18/18.
//  Copyright © 2018 Jeremy Evans. All rights reserved.
//

import Foundation
import UIKit

struct CurrentSession: Decodable {
    public var User: User
    public let WaterLog: [Water]
    public let StepLog: [Pedometer]
    public var Subscription: Subscription_X_User
}

