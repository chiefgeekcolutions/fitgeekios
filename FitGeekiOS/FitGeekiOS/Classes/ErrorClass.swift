//
//  ErrorClass.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 1/18/18.
//  Copyright © 2018 Jeremy Evans. All rights reserved.
//

import Foundation
import UIKit

struct ErrorClass: Error {
    public var err_message: String?
}
