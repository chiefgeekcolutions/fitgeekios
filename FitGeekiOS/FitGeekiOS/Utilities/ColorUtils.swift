//
//  ColorUtils.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 10/30/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import Foundation
import UIKit

class ColorUtils {
    public let ChiefGeekOrange = UIColor(red: 255/255, green: 64/255, blue: 0/255, alpha: 1.0)
    public let ChiefGeekOrangeSelected = UIColor(red: 255/255, green: 64/255, blue: 0/255, alpha: 0.5)
    public let DefaultGray = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
    public let ShadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
    public let LinkColor = UIColor(red: 0/255, green: 27/255, blue: 61/255, alpha: 1.0)
    public let LinkColorSelected = UIColor(red: 0/255, green: 27/255, blue: 61/255, alpha: 0.5)
}
