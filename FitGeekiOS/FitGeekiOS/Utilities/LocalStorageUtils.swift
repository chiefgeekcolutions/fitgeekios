//
//  LocalStorageUtils.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 12/9/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import Foundation
import UIKit

struct LocalStorage {
    public static func init_store(session: CurrentSession) {
        // Stores Local Instance of user to the phone for the first time
        // Called when user is logged in
        
    }
    
    public static func updateCurrentSession() {
        // Called everytime a log` is synced or there is a change to the users profile
    }
}
