//
//  ApiUtils.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 1/18/18.
//  Copyright © 2018 Jeremy Evans. All rights reserved.
//

//import Foundation
import UIKit

class ApiUtils {
    
    private static let path: String! = Bundle.main.path(forResource: "FitGeekApi", ofType: "plist")
    private static var FitGeekInfo: Dictionary<String, Any>! = NSDictionary(contentsOfFile: path) as? Dictionary
    private static let encoder = JSONEncoder()
    private static let ApiUrl: String! = FitGeekInfo!["apiurl"] as? String
    
    /*private static func getApiUri() -> String {
        // Always set Plist Info
        getAppInfo()
        // Get Url from
        guard let ApiUrl = FitGeekInfo!["apiurl"] as? String else {
            return ""
        }
        return ApiUrl
    }*/
    
    // Get Set FitGeekApiInfo
    /*private static func getAppInfo() {
        // Get PList Info for app
        if let path = Bundle.main.path(forResource: "FitGeekApi", ofType: "plist") {
            //print(path)
            ApiUtils.FitGeekInfo = NSDictionary(contentsOfFile: path) as? Dictionary
        }
    }*/
    
    public static func SignUpUser(newUser: User) -> ErrorClass {
        // Init
        var oEr = ErrorClass()
        // Create Json Object
        // Call Service if object exists
        if let jsonData = newUser.toJSON() {
            let url = URL(string: ApiUrl + "Register")!
                // Serialize JsonObject to String for post Request
                //let jsonData = try JSONSerialization.data(withJSONObject: rawJson, options: [])
                var request = URLRequest(url: url)
                // Set Request Parameters
                request.httpMethod = "POST"
                request.httpBody = jsonData as? Data // Post Body
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                
                // Call Web Service URL
                let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                    do {
                        if error != nil {
                            if let err = error {
                                oEr.err_message = err.localizedDescription
                                //Config.LastErr = oEr//return error?.localizedDescription
                            } else {
                                oEr.err_message = "Failed to sign up user via the API."
                                //Config.LastErr = oEr
                            }
                        }
                        // Protect getting json data
                        guard let responseData = data else { return }
                        
                        //Decode retrived data with JSONDecoder and assing type of Article object
                        let oCurrentSession = try JSONDecoder().decode(CurrentSession.self, from: responseData)
                        
                        LocalStorage.init_store(session: oCurrentSession)
                        
                    } catch let jsonError {
                        oEr.err_message = jsonError.localizedDescription
                        
                    }
                })//.dataTask(with: request)
                task.resume()
        }
        print(oEr)
        return oEr
    }
    
}
