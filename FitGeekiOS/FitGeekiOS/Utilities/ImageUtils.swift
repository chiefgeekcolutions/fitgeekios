//
//  ImageUtils.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 11/1/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import Foundation
import UIKit

class ImageUtils {
    // Helper Functions
    public func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        image.draw(in: CGRect(origin: CGPoint.zero, size: CGSize(width: newSize.width, height: newSize.height)))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}
