//
//  ProfilePicImageView.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 12/11/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import UIKit

class ProfilePicImageView: UIImageView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    // Conveniienve
    convenience init(frame: CGRect, img: UIImage) {
        self.init(frame: frame)
        
        // Setup Profile Pic Image
        image = img // set image
        layer.cornerRadius = frame.size.width / 2
        clipsToBounds = true
        layer.borderWidth = 2
        layer.borderColor = UIColor.black.cgColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
