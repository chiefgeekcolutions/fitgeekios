//
//  BottomBorderTextField.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 11/1/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import UIKit

class BottomBorderTextField: UITextField {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    convenience init(frame: CGRect, PHolderTxt: String) {
        // Initialize self with frame given
        self.init(frame: frame)
        
        // Bottom Border
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = UIColor.darkGray.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
        self.translatesAutoresizingMaskIntoConstraints = false
        // Bottom Border end
        
        // Set Placeholer text
        self.placeholder = PHolderTxt
    }
    
    // Set Padding for text
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, UIEdgeInsetsMake(0, 15, 0, 15))
    }
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, UIEdgeInsetsMake(0, 15, 0, 15))
    }
}
