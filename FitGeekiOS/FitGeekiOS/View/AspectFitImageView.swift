//
//  OrangeHeadImageView.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 10/29/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import UIKit

class AspectFitImageView: UIImageView {

    override init(image: UIImage?) {
        // Do what you need with image
        super.init(image: image)
        // Set defaults during init
        self.contentMode = .scaleAspectFit
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // do nothing
    }
}
