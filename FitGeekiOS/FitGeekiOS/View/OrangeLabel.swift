//
//  OrangeLabel.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 11/1/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import UIKit

class OrangeLabel: UILabel {
    
    let oColor = ColorUtils()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //self.tintColor = oColor.ChiefGeekOrange
        self.textColor = oColor.ChiefGeekOrange
        self.contentMode = .scaleAspectFit
        self.font = UIFont.systemFont(ofSize: 14, weight: .regular)//UIFont(descriptor: , size: <#T##CGFloat#>)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
