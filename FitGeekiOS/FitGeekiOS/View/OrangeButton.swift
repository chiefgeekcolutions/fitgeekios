//
//  OrangeButton.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 10/30/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import UIKit

class OrangeButton: UIButton {
    let Colors = ColorUtils()

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(frame: CGRect, title: String) {
        self.init(frame: frame)
        // Set Title
        self.setTitle(title, for: .normal)
        self.setTitleColor(Colors.ChiefGeekOrange, for: .normal)
        
        // Set Border
        self.layer.borderWidth = 1.0
        self.layer.borderColor = Colors.ChiefGeekOrange.cgColor
        self.layer.cornerRadius = 10.0
        
        // Shadow
        self.layer.shadowColor = Colors.ShadowColor.cgColor
        self.layer.masksToBounds = true
        self.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.layer.shadowRadius = 5
        
        // Touch
        self.setTitleColor(Colors.ChiefGeekOrangeSelected, for: .highlighted)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
