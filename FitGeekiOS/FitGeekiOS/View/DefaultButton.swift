//
//  DefaultButton.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 10/30/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import UIKit

class DefaultButton: UIButton {
    let Colors = ColorUtils()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(frame: CGRect, title: String) {
        self.init(frame: frame)
        // Set Title
        self.setTitle(title, for: .normal)
        self.setTitleColor(Colors.ChiefGeekOrange, for: .normal)
        self.backgroundColor = Colors.DefaultGray
        
        // Set Border
        /*self.layer.borderWidth = 1.0
        self.layer.borderColor = Colors.ChiefGeekOrange.cgColor
        */
        
        // Corner Raidus
        self.layer.cornerRadius = 10.0
        
        // Shadow
        /*self.layer.shadowColor = Colors.ShadowColor.cgColor
        self.layer.shadowOpacity = 1.0
         self.layer.masksToBounds = true
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 5*/
        self.layer.shadowColor = Colors.ShadowColor.cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize(width: 1, height: 1)//CGSize.zero
        self.layer.shadowRadius = 3
        
        //self.layer.shadowPath = UIBezierPath(//UIBezierPath(rect: self.bounds).cgPath
        //self.layer.shouldRasterize = true
        
        // Touch
        self.setTitleColor(Colors.ChiefGeekOrangeSelected, for: .highlighted)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
