//
//  SignupPageTwoCell.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 12/9/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import UIKit

class SignupPageTwoCell: UICollectionViewCell {
    let oImgUtil = ImageUtils()
    
    // Controls
    let logoImageView: AspectFitImageView = {
        let imageView = AspectFitImageView(image: #imageLiteral(resourceName: "DarkHeadLogo"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let stepLabel: OrangeLabel = {
        let label = OrangeLabel(frame: CGRect(x: 0, y: 0, width: 35, height: 16))
        label.text = "Step Goal:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let currentWeightLabel: OrangeLabel = {
        let label = OrangeLabel(frame: CGRect(x: 0, y: 0, width: 35, height: 16))
        label.text = "Current Weight:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let WeightGoalLabel: OrangeLabel = {
        let label = OrangeLabel(frame: CGRect(x: 0, y: 0, width: 35, height: 16))
        label.text = "Weight Goal:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let GoalDateLabel: OrangeLabel = {
        let label = OrangeLabel(frame: CGRect(x: 0, y: 0, width: 100, height: 16))
        label.text = "Password:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let stepTxt: BottomBorderTextField = {
        let rect = CGRect(x: 0, y: 0, width: 300, height: 45)
        let txtField = BottomBorderTextField(frame: rect, PHolderTxt: "10,000")
        return txtField
    }()
    
    let weightTxtField: BottomBorderTextField = {
        let rect = CGRect(x: 0, y: 0, width: 300, height: 45)
        let txtField = BottomBorderTextField(frame: rect, PHolderTxt: "195")
        return txtField
    }()
    
    let weightGoalTxtField: BottomBorderTextField = {
        let rect = CGRect(x: 0, y: 0, width: 300, height: 45)
        let txtField = BottomBorderTextField(frame: rect, PHolderTxt: "175")
        return txtField
    }()
    // TODO: Date Picker!!!!
    let goalDateField: BottomBorderTextField = {
        let rect = CGRect(x: 0, y: 0, width: 300, height: 45)
        let txtField = BottomBorderTextField(frame: rect, PHolderTxt: "10/15/2018")
        return txtField
    }()
    // Controls End
    
    // Create and customize cell
    override init(frame: CGRect) {
        super.init(frame: frame)
        // Set background
        let bgImage = oImgUtil.imageWithImage(image: UIImage(named: "LoginBG.png")!, scaledToSize: CGSize(width: bounds.width, height: bounds.height))
        
        // Set Background
        self.backgroundColor = UIColor(patternImage: bgImage)
        
        
        // Add Views to Subview
        addSubview(logoImageView)
        addSubview(stepLabel)
        addSubview(stepTxt)
        addSubview(currentWeightLabel)
        addSubview(weightTxtField)
        addSubview(WeightGoalLabel)
        addSubview(weightGoalTxtField)
        addSubview(GoalDateLabel)
        addSubview(goalDateField)
        
        
        // Setup Layout
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("inti(coder) has not been implemented")
    }
    
    func setupLayout(){
        // Logo
        logoImageView.centerXAnchor.constraint(equalTo: centerXAnchor, constant: -10).isActive = true
        logoImageView.topAnchor.constraint(equalTo: topAnchor, constant: 50).isActive = true
        logoImageView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        logoImageView.heightAnchor.constraint(equalToConstant: 96).isActive = true
        
        // First Name label
        stepLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 40).isActive = true
        stepLabel.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 35).isActive = true
        stepLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        stepLabel.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        // First Name Textbox
        stepTxt.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        stepTxt.topAnchor.constraint(equalTo: stepLabel.bottomAnchor, constant: 0).isActive = true
        stepTxt.widthAnchor.constraint(equalToConstant: 300).isActive = true
        stepTxt.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        // Last Name label
        currentWeightLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 40).isActive = true
        currentWeightLabel.topAnchor.constraint(equalTo: stepTxt.bottomAnchor, constant: 35).isActive = true
        currentWeightLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        currentWeightLabel.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        // Last Name Textbox
        weightTxtField.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        weightTxtField.topAnchor.constraint(equalTo: currentWeightLabel.bottomAnchor, constant: 0).isActive = true
        weightTxtField.widthAnchor.constraint(equalToConstant: 300).isActive = true
        weightTxtField.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        // Email label
        WeightGoalLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 40).isActive = true
        WeightGoalLabel.topAnchor.constraint(equalTo: weightTxtField.bottomAnchor, constant: 35).isActive = true
        WeightGoalLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        WeightGoalLabel.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        // Username Textbox
        weightGoalTxtField.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        weightGoalTxtField.topAnchor.constraint(equalTo: WeightGoalLabel.bottomAnchor, constant: 0).isActive = true
        weightGoalTxtField.widthAnchor.constraint(equalToConstant: 300).isActive = true
        weightGoalTxtField.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        // Password Lable
        GoalDateLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 40).isActive = true
        GoalDateLabel.topAnchor.constraint(equalTo: weightGoalTxtField.bottomAnchor, constant: 35).isActive = true
        GoalDateLabel.widthAnchor.constraint(equalToConstant: 300).isActive = true
        GoalDateLabel.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        // Password TxtField
        goalDateField.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        goalDateField.topAnchor.constraint(equalTo: GoalDateLabel.bottomAnchor, constant: 0).isActive = true
        goalDateField.widthAnchor.constraint(equalToConstant: 300).isActive = true
        goalDateField.heightAnchor.constraint(equalToConstant: 45).isActive = true
    }
}
