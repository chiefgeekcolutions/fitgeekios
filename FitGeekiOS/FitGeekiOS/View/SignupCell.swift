//
//  SignupPageCell.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 11/5/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import UIKit

struct newFrame {
    public static var width: CGFloat = 0
    public static var height: CGFloat = 0
}

class SignupCell: UICollectionViewCell {
    
    // Static Variables
    var step: Int = 0

    let oImgUtil = ImageUtils()
    
    // Page 2
    let stepLabel: OrangeLabel = {
        let label = OrangeLabel(frame: CGRect(x: 0, y: 0, width: 35, height: 16))
        label.text = "Step Goal:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let currentWeightLabel: OrangeLabel = {
        let label = OrangeLabel(frame: CGRect(x: 0, y: 0, width: 35, height: 16))
        label.text = "Current Weight:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let WeightGoalLabel: OrangeLabel = {
        let label = OrangeLabel(frame: CGRect(x: 0, y: 0, width: 35, height: 16))
        label.text = "Weight Goal:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let GoalDateLabel: OrangeLabel = {
        let label = OrangeLabel(frame: CGRect(x: 0, y: 0, width: 100, height: 16))
        label.text = "Password:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let stepTxt: BottomBorderTextField = {
        let rect = CGRect(x: 0, y: 0, width: 300, height: 45)
        let txtField = BottomBorderTextField(frame: rect, PHolderTxt: "10,000")
        txtField.translatesAutoresizingMaskIntoConstraints = false
        return txtField
    }()
    
    let weightTxtField: BottomBorderTextField = {
        let rect = CGRect(x: 0, y: 0, width: 300, height: 45)
        let txtField = BottomBorderTextField(frame: rect, PHolderTxt: "195")
        txtField.translatesAutoresizingMaskIntoConstraints = false
        return txtField
    }()
    
    let weightGoalTxtField: BottomBorderTextField = {
        let rect = CGRect(x: 0, y: 0, width: 300, height: 45)
        let txtField = BottomBorderTextField(frame: rect, PHolderTxt: "175")
        txtField.translatesAutoresizingMaskIntoConstraints = false
        return txtField
    }()
    // TODO: Date Picker!!!!
    let goalDateField: BottomBorderTextField = {
        let rect = CGRect(x: 0, y: 0, width: 300, height: 45)
        let txtField = BottomBorderTextField(frame: rect, PHolderTxt: "10/15/2018")
        txtField.translatesAutoresizingMaskIntoConstraints = false
        return txtField
    }()
    // Page 2 Ends
    
    // Page 3
    let profilePic: ProfilePicImageView = {
        let rect = CGRect(x: 0, y: 0, width: 148, height: 148)
        let imgView = ProfilePicImageView(frame: rect, img: #imageLiteral(resourceName: "sillouete"))
        imgView.translatesAutoresizingMaskIntoConstraints = false
        return imgView
    }()
    
    let tapToEditLbl: OrangeLabel = {
        let label = OrangeLabel(frame: CGRect(x: 0, y: 0, width: 35, height: 16))
        label.text = "Tap to Edit"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = NSTextAlignment.center
        return label
    }()
    
    let genderLbl: OrangeLabel = {
        let label = OrangeLabel(frame: CGRect(x: 0, y: 0, width: 35, height: 16))
        label.text = "Gender:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let bioLbl: OrangeLabel = {
        let label = OrangeLabel(frame: CGRect(x: 0, y: 0, width: 100, height: 16))
        label.text = "Bio:"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let genderTxtBox: BottomBorderTextField = {
        let rect = CGRect(x: 0, y: 0, width: 300, height: 45)
        let txtField = BottomBorderTextField(frame: rect, PHolderTxt: "Please Select")
        txtField.translatesAutoresizingMaskIntoConstraints = false
        txtField.loadDropdownData(data: ["", "Male", "Female"])
        return txtField
    }()
    
    let bioText: BioTextView = {
        let rect = CGRect(x: 0, y: 0, width: 238, height: 131)
        let container = NSTextContainer(size: CGSize(width: rect.width, height: rect.height))
        let textView = BioTextView(frame: rect, textContainer: container)
        //let txtField = BottomBorderTextField(frame: rect, PHolderTxt: "10,000")
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    // Page 3 Ends
    // Controls End
    
    // Create and customize cell
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // Set width/height
        newFrame.width = frame.width
        newFrame.height = frame.height
        
        
        // Choose Layout
        layoutPicker()
    }
    
    // Sets Layout
    func layoutPicker(){
        // Clear screen
        removeSubViews()
        // reset layout
        if(self.step == 0) {
            //setupLayoutFirst()
        } else if (self.step == 1) {
            //setupLayoutSecond()
        } else if(self.step == 2) {
           // setupLayoutThird()
        }
    }
    
    // Remove all views before layout
    private func removeSubViews() {
        for view in subviews {
            view.removeFromSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("inti(coder) has not been implemented")
    }
    
    /*func setupLayoutSecond(){
        
        // Add Views to Subview
        addSubview(logoImageView)
        addSubview(stepLabel)
        addSubview(stepTxt)
        addSubview(currentWeightLabel)
        addSubview(weightTxtField)
        addSubview(WeightGoalLabel)
        addSubview(weightGoalTxtField)
        addSubview(GoalDateLabel)
        addSubview(goalDateField)
        
        // Logo
        logoImageView.centerXAnchor.constraint(equalTo: centerXAnchor, constant: -10).isActive = true
        logoImageView.topAnchor.constraint(equalTo: topAnchor, constant: 50).isActive = true
        logoImageView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        logoImageView.heightAnchor.constraint(equalToConstant: 96).isActive = true
        
        // First Name label
        stepLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 40).isActive = true
        stepLabel.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 35).isActive = true
        stepLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        stepLabel.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        // First Name Textbox
        stepTxt.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        stepTxt.topAnchor.constraint(equalTo: stepLabel.bottomAnchor, constant: 0).isActive = true
        stepTxt.widthAnchor.constraint(equalToConstant: 300).isActive = true
        stepTxt.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        // Last Name label
        currentWeightLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 40).isActive = true
        currentWeightLabel.topAnchor.constraint(equalTo: stepTxt.bottomAnchor, constant: 35).isActive = true
        currentWeightLabel.widthAnchor.constraint(equalToConstant: 125).isActive = true
        currentWeightLabel.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        // Last Name Textbox
        weightTxtField.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        weightTxtField.topAnchor.constraint(equalTo: currentWeightLabel.bottomAnchor, constant: 0).isActive = true
        weightTxtField.widthAnchor.constraint(equalToConstant: 300).isActive = true
        weightTxtField.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        // Email label
        WeightGoalLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 40).isActive = true
        WeightGoalLabel.topAnchor.constraint(equalTo: weightTxtField.bottomAnchor, constant: 35).isActive = true
        WeightGoalLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        WeightGoalLabel.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        // Username Textbox
        weightGoalTxtField.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        weightGoalTxtField.topAnchor.constraint(equalTo: WeightGoalLabel.bottomAnchor, constant: 0).isActive = true
        weightGoalTxtField.widthAnchor.constraint(equalToConstant: 300).isActive = true
        weightGoalTxtField.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        // Password Lable
        GoalDateLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 40).isActive = true
        GoalDateLabel.topAnchor.constraint(equalTo: weightGoalTxtField.bottomAnchor, constant: 35).isActive = true
        GoalDateLabel.widthAnchor.constraint(equalToConstant: 300).isActive = true
        GoalDateLabel.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        // Password TxtField
        goalDateField.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        goalDateField.topAnchor.constraint(equalTo: GoalDateLabel.bottomAnchor, constant: 0).isActive = true
        goalDateField.widthAnchor.constraint(equalToConstant: 300).isActive = true
        goalDateField.heightAnchor.constraint(equalToConstant: 45).isActive = true
    }
    
    func setupLayoutThird(){
        
        // Add Views to Subview
       // addSubview(logoImageView)
        addSubview(profilePic)
        addSubview(tapToEditLbl)
        addSubview(genderLbl)
        addSubview(bioLbl)
        addSubview(genderTxtBox)
        addSubview(bioText)
        
        // Profile Pic
        profilePic.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        profilePic.topAnchor.constraint(equalTo: topAnchor, constant: 75).isActive = true
        profilePic.widthAnchor.constraint(equalToConstant: 148).isActive = true
        profilePic.heightAnchor.constraint(equalToConstant: 148).isActive = true
        
        // Tap to Edit
        tapToEditLbl.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        tapToEditLbl.topAnchor.constraint(equalTo: profilePic.bottomAnchor, constant: 5).isActive = true
        tapToEditLbl.widthAnchor.constraint(equalToConstant: 100).isActive = true
        tapToEditLbl.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        // gender label
        genderLbl.leftAnchor.constraint(equalTo: leftAnchor, constant: 40).isActive = true
        genderLbl.topAnchor.constraint(equalTo: tapToEditLbl.bottomAnchor, constant: 35).isActive = true
        genderLbl.widthAnchor.constraint(equalToConstant: 100).isActive = true
        genderLbl.heightAnchor.constraint(equalToConstant: 16).isActive = true
        
        // gender txt box
        genderTxtBox.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
        genderTxtBox.topAnchor.constraint(equalTo: genderLbl.bottomAnchor, constant: 0).isActive = true
        genderTxtBox.widthAnchor.constraint(equalToConstant: 300).isActive = true
        genderTxtBox.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        // bio label
        bioLbl.leftAnchor.constraint(equalTo: leftAnchor, constant: 40).isActive = true
        bioLbl.topAnchor.constraint(equalTo: genderTxtBox.bottomAnchor, constant: 15).isActive = true
        bioLbl.widthAnchor.constraint(equalToConstant: 300).isActive = true
        bioLbl.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        // bio text
        bioText.leftAnchor.constraint(equalTo: leftAnchor, constant: 40).isActive = true
        bioText.topAnchor.constraint(equalTo: bioLbl.bottomAnchor, constant: 35).isActive = true
        bioText.widthAnchor.constraint(equalToConstant: 238).isActive = true
        bioText.heightAnchor.constraint(equalToConstant: 131).isActive = true
    }
    
    func fixView() {
        
    }*/
}
