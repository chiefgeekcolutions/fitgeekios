//
//  LinkButton.swift
//  FitGeekiOS
//
//  Created by Jeremy Evans on 11/1/17.
//  Copyright © 2017 Jeremy Evans. All rights reserved.
//

import UIKit

class LinkButton: UIButton {
    let Colors = ColorUtils()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(frame: CGRect, text: String) {
        self.init(frame: frame)
        
        // Set Title
        self.setTitle(text, for: .normal)
        self.setTitleColor(Colors.LinkColor, for: .normal)
        
        // On Touch
        self.setTitleColor(Colors.LinkColorSelected, for: .highlighted)
        
        self.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
